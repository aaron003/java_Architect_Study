package B_Mode.abstractFactory;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/11 21:52
 * @Description : 海尔电视机
 */
public interface IHaierTV {
    void produce();
}
