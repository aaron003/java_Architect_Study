package B_Mode.abstractFactory;

import B_Mode.factoryMethod.IHaierElectric;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/10 18:24
 * @Description : 海尔电器工厂总接口
 */
public interface IHaierAppliancesFactory {
    public IHaierAirConditioning dispatchAirConditioning();

    public IHaierRefrigerator dispatchRefrigerator();

    public IHaierTV dispatchTV();

    public IHaierWashingMachine dispatchWashingMachine();
}
