package B_Mode.abstractFactory;

import B_Mode.factoryMethod.IHaierElectric;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/11 21:54
 * @Description : 海尔传统家电工厂类
 */
public class HaierTAFactory implements IHaierAppliancesFactory{
    public IHaierAirConditioning dispatchAirConditioning() {
        return new HaierTAAirConditioning();
    }

    public IHaierRefrigerator dispatchRefrigerator() {
        return new HaierTARefrigerator();
    }

    public IHaierTV dispatchTV() {
        return new HaierTATV();
    }

    public IHaierWashingMachine dispatchWashingMachine() {
        return new HaierTAWashingMachine();
    }
}
