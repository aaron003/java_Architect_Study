package B_Mode.abstractFactory;

import B_Mode.factoryMethod.IHaierElectric;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/11 21:54
 * @Description : 海尔智能家电工厂类
 */
public class HaierSAFactory implements IHaierAppliancesFactory{
    public IHaierAirConditioning dispatchAirConditioning() {
        return new HaierSAAirConditioning();
    }

    public IHaierRefrigerator dispatchRefrigerator() {
        return new HaierSARefrigerator();
    }

    public IHaierTV dispatchTV() {
        return new HaierSATV();
    }

    public IHaierWashingMachine dispatchWashingMachine() {
        return new HaierSAWashingMachine();
    }
}
