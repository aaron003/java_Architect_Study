package B_Mode.abstractFactory;

/**
 * @Author : Aaron
 * @Date : Created in 2020/04/10 23:04
 * @Description : 抽象工厂测试类
 */
public class Test {
    public static void main(String[] args) {
        HaierSAFactory saFactory = new HaierSAFactory();
        HaierTAFactory taFactory = new HaierTAFactory();
        IHaierAirConditioning iHaierAirConditioning = taFactory.dispatchAirConditioning();
        iHaierAirConditioning.produce();
        System.out.println("******************************");
        IHaierTV iHaierTV = saFactory.dispatchTV();
        iHaierTV.produce();
    }
}
