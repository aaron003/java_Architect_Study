package B_Mode.abstractFactory;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/11 21:52
 * @Description : 海尔洗衣机
 */
public interface IHaierWashingMachine {
    void produce();
}
