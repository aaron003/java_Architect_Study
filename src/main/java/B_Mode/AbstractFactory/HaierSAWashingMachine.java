package B_Mode.abstractFactory;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/11 21:52
 * @Description : 海尔洗衣机
 */
public class HaierSAWashingMachine implements IHaierWashingMachine{

    public void produce() {
        System.out.println("海尔智能洗衣机生产线开始运作...");
        System.out.println("海尔智能洗衣机生产开始...");
        System.out.println("海尔智能洗衣机生产完毕!!!");
    }
}
