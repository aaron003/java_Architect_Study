package B_Mode.decorator.v1;

/**
 * @Author : Aaron
 * @Date : Created in 2020/5/6 18:18
 * @Description : 基础装饰器
 */
public class BaseCar extends Car {
    protected String getMsg() {
        return "组装汽车零件:添加车架";
    }

    protected double getSchedule() {
        return 1;
    }
}
