package B_Mode.decorator.v1;

/**
 * @Author : Aaron
 * @Date : Created in 2020/5/6 18:03
 * @Description : 汽车装饰器
 */
public class CarDecorator extends BaseCar {

    Car car;

    public CarDecorator(Car car) {
        this.car = car;
    }

    protected String getMsg() {
        return this.car.getMsg();
    }

    protected double getSchedule() {
        return this.car.getSchedule();
    }
}
