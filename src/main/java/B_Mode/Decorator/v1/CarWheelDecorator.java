package B_Mode.decorator.v1;

/**
 * @Author : Aaron
 * @Date : Created in 2020/5/6 18:00
 * @Description : 汽车轮子装饰器
 */
public class CarWheelDecorator extends CarDecorator {

    public CarWheelDecorator(Car car) {
        super(car);
    }

    protected String getMsg() {
        return super.getMsg() + "添加一个汽车轮子";
    }

    protected double getSchedule() {
        return super.getSchedule() + 1 ;
    }
}
