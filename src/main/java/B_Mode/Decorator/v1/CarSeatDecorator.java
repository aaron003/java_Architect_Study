package B_Mode.decorator.v1;

/**
 * @Author : Aaron
 * @Date : Created in 2020/5/6 18:00
 * @Description : 汽车座椅装饰器
 */
public class CarSeatDecorator extends CarDecorator {


    public CarSeatDecorator(Car car) {
        super(car);
    }

    protected String getMsg() {
        return super.getMsg() + "添加一个汽车座椅";
    }

    protected double getSchedule() {
        return super.getSchedule() + 1 ;
    }
}
