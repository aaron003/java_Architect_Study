package B_Mode.decorator.v1;

/**
 * @Author : Aaron
 * @Date : Created in 2020/5/6 18:08
 * @Description : 装饰器模式测试类
 */
public class Test {
    public static void main(String[] args) {
        Car car ;
        car = new BaseCar();
        car = new CarSeatDecorator(car);
        car = new CarSeatDecorator(car);
        car = new CarWheelDecorator(car);

        System.out.println(car.getMsg() + ",完成度:" + car.getSchedule() + "%");
    }
}
