package B_Mode.decorator.v1;

/**
 * @Author : Aaron
 * @Date : Created in 2020/5/6 17:57
 * @Description : 汽车装饰器
 */
public abstract class Car {
    protected abstract String getMsg();

    protected abstract double getSchedule();
}
