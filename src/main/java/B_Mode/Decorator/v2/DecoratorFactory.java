package B_Mode.decorator.v2;

/**
 * @Author : Aaron
 * @Date : Created in 2020/5/6 19:10
 * @Description : TODO
 */
public class DecoratorFactory {
    public NavigationBar getNavigationBarContext(String status){
        NavigationBar navigationBar;
        navigationBar = new BaseNavigationBar();
        navigationBar = new CommodityDecorator(navigationBar);
        navigationBar = new ArticleDecorator(navigationBar);
        if ("1".equals(status)){
            navigationBar = new WorkDecorator(navigationBar);
            navigationBar = new TestBankDecorator(navigationBar);
            navigationBar = new GrowthWallDecorator(navigationBar);
        }
        navigationBar = new FineClassDecorator(navigationBar);
        navigationBar = new BubbleDecorator(navigationBar);
        navigationBar = new MallDecorator(navigationBar);

        return navigationBar;
    }
}
