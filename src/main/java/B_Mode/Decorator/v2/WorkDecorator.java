package B_Mode.decorator.v2;

/**
 * @Author : Aaron
 * @Date : Created in 2020/5/6 19:01
 * @Description : TODO
 */
public class WorkDecorator extends BaseDecorator {


    public WorkDecorator(NavigationBar navigationBar) {
        super(navigationBar);
    }

    protected String getMsg() {
        return super.getMsg() + "作业 ";
    }
}
