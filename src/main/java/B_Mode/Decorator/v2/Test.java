package B_Mode.decorator.v2;


/**
 * @Author : Aaron
 * @Date : Created in 2020/5/6 19:18
 * @Description : TODO
 */
public class Test {
    public static void main(String[] args) {
        DecoratorFactory decoratorFactory = new DecoratorFactory();
        NavigationBar navigationBar = decoratorFactory.getNavigationBarContext("1");
        System.out.println(navigationBar.getMsg());
    }
}
