package B_Mode.decorator.v2;

/**
 * @Author : Aaron
 * @Date : Created in 2020/5/6 19:06
 * @Description : 基础
 */
public class BaseDecorator extends BaseNavigationBar {
    NavigationBar navigationBar;

    public BaseDecorator(NavigationBar navigationBar) {
        this.navigationBar = navigationBar;
    }

    protected String getMsg() {
        return this.navigationBar.getMsg();
    }
}
