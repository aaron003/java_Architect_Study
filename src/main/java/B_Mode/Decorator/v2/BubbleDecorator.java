package B_Mode.decorator.v2;

/**
 * @Author : Aaron
 * @Date : Created in 2020/5/6 19:01
 * @Description : TODO
 */
public class BubbleDecorator extends BaseDecorator {
    public BubbleDecorator(NavigationBar navigationBar) {
        super(navigationBar);
    }

    protected String getMsg() {
        return super.getMsg() +  "冒泡 ";
    }
}
