package B_Mode.decorator.v2;

/**
 * @Author : Aaron
 * @Date : Created in 2020/5/6 19:01
 * @Description : 文章
 */
public class ArticleDecorator extends BaseDecorator {


    public ArticleDecorator(NavigationBar navigationBar) {
        super(navigationBar);
    }

    protected String getMsg() {
        return  super.getMsg() +  "文章 ";
    }
}
