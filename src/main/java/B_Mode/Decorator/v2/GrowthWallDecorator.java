package B_Mode.decorator.v2;

/**
 * @Author : Aaron
 * @Date : Created in 2020/5/6 19:01
 * @Description : TODO
 */
public class GrowthWallDecorator extends BaseDecorator {


    public GrowthWallDecorator(NavigationBar navigationBar) {
        super(navigationBar);
    }

    protected String getMsg() {
        return super.getMsg() + "成长墙 ";
    }
}
