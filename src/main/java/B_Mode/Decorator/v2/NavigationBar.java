package B_Mode.decorator.v2;

/**
 * @Author : Aaron
 * @Date : Created in 2020/5/6 18:59
 * @Description : 导航栏
 */
public abstract class NavigationBar {
    protected abstract String getMsg();
}
