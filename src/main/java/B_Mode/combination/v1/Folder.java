package B_Mode.combination.v1;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author : Aaron
 * @Date : Created in 2020/5/10 15:03
 * @Description : 文件夹
 */
public class Folder extends Direcotry {
    private List<Direcotry> dirs;

    private Integer level;

    public Folder(String name,int level) {
        super(name);
        this.level = level;
        dirs = new ArrayList<Direcotry>();
    }

    public void addChild(Direcotry dir){
        this.dirs.add(dir);
    }

    public void remove (Direcotry dir){
        this.dirs.remove(dir);
    }

    public Direcotry get(int index){
        return this.dirs.get(index);
    }

    public void show() {
        System.out.println(this.name);

        for (Direcotry d : dirs){
            if (this.level != null){
                for (int i = 0 ; i<this.level; i ++){
                    System.out.print("  ");
                }
                for (int i = 0 ; i<this.level; i ++){
                    if (i == 0 ){
                        System.out.print("+");
                    }
                }
                System.out.print("-");
            }
            d.show();
        }
    }
}
