package B_Mode.combination.v2;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author : Aaron
 * @Date : Created in 2020/5/10 15:03
 * @Description : 文件夹
 */
public class Folder extends Direcotry {
    private List<Direcotry> dirs = new ArrayList<Direcotry>();

    public Folder(String name) {
        super(name);
    }

    public void addChild(Direcotry dir) {
        this.dirs.add(dir);
    }

    public void remove(Direcotry dir) {
        this.dirs.remove(dir);
    }

    public Direcotry get(int index) {
        return this.dirs.get(index);
    }

    public void show(String preStr) {
        System.out.println(preStr + "+" + this.name);
        if (dirs != null) {
            preStr += " ";
            for (Direcotry d : dirs) {
                d.show(preStr);
            }
        }
    }
}
