package B_Mode.combination.v2;


/**
 * @Author : Aaron
 * @Date : Created in 2020/5/10 15:17
 * @Description : 组合模式测试类
 */
public class Test {
    public static void main(String[] args) {
        Folder root = new Folder("D;");
        File idea = new File("idea.exe");
        File git = new File("git.exe");
        root.addChild(idea);
        root.addChild(git);
        Folder game = new Folder("game");
        Folder game1 = new Folder("1111");
        game.addChild(game1);
        root.addChild(game);

        root.show(" ");
    }
}
