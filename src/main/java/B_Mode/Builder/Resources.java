package B_Mode.builder;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/19 19:26
 * @Description : 资源类
 */
public class Resources {

    public Monster getMonster() {
        return new Monster(1, 10, 10, 100, 50);
    }

    public Road getRoad() {
        return new Road(10,100,20,20);
    }

    public Tree getTree() {
        return new Tree("绿色",30,30);
    }
}
