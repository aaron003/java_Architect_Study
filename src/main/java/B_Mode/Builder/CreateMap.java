package B_Mode.builder;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/19 19:36
 * @Description : 创建地图实现类
 */
public class CreateMap implements Build {
    private Map map = new Map();
    public Build buildMonster(Monster monster) {
        map.setMonster(monster);
        return this;
    }

    public Build buildRoad(Road road) {
        map.setRoad(road);
        return this;
    }

    public Build buildTree(Tree tree) {
        map.setTree(tree);
        return this;
    }

    public Build buildPx(String px) {
        map.setPx(px);
        return this;
    }

    public Build buildMusic(String music) {
        map.setMusic(music);
        return this;
    }

    public Map getMap() {
        return map;
    }
}
