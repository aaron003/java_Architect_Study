package B_Mode.builder;

import lombok.Data;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/19 19:12
 * @Description : 野怪类
 */
@Data
public class Monster {
    private int level;//等级
    private int x;
    private int y;
    private int power;//力量
    private int defense;//防御

    public Monster(int level, int x, int y, int power, int defense) {
        this.level = level;
        this.x = x;
        this.y = y;
        this.power = power;
        this.defense = defense;
    }
}
