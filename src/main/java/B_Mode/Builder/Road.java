package B_Mode.builder;

import lombok.Data;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/19 19:23
 * @Description : 道路类
 */
@Data
public class Road {
    private int width;//路宽
    private int length;//路长
    private int x;
    private int y;

    public Road(int width, int length, int x, int y) {
        this.width = width;
        this.length = length;
        this.x = x;
        this.y = y;
    }
}
