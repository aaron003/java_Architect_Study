package B_Mode.builder;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/19 19:15
 * @Description : 建造者模式测试类
 */
public class Test {
    public static void main(String[] args) {
        Builder builder = new Builder();
        Map map = builder.createMap("50%",false);
        System.out.println(map);
    }
}
