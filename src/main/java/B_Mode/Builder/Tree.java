package B_Mode.builder;

import lombok.Data;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/19 19:22
 * @Description : 树类
 */
@Data
public class Tree {
    private String color;//树颜色
    private int x;
    private int y;

    public Tree(String color, int x, int y) {
        this.color = color;
        this.x = x;
        this.y = y;
    }
}
