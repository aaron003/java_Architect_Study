package B_Mode.builder;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/19 19:38
 * @Description : TODO
 */
public class Builder {
    Build build = new CreateMap();
    Resources resources = new Resources();

    public Map createMap(String px,boolean ishasMusic){
        build.buildMonster(resources.getMonster())
                .buildRoad(resources.getRoad())
                .buildTree(resources.getTree())
                .buildPx(px)
                .buildMusic(ishasMusic ? "有音乐":"无音乐");
        return build.getMap();
    }
}
