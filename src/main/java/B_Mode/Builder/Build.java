package B_Mode.builder;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/19 19:32
 * @Description : TODO
 */
public interface Build {
    Build buildMonster(Monster monster);
    Build buildRoad(Road road);
    Build buildTree(Tree tree);
    Build buildPx(String px);
    Build buildMusic(String music);
    Map getMap();
}
