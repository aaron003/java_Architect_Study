package B_Mode.builder;

import lombok.Data;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/19 19:35
 * @Description : 构建地图类
 */
@Data
public class Map {
    private Monster monster;
    private Road road;
    private Tree tree;
    private String px;
    private String music;
}
