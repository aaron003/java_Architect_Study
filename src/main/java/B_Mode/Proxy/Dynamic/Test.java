package B_Mode.proxy.Dynamic;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/29 21:48
 * @Description : 动态代理测试类
 */
public class Test {
    public static void main(String[] args) {
        FinaService finaService = new FinaService();
        IFina iFina = finaService.getInstance(new FinaLoan());
        iFina.insertFinaLond();
        iFina.insertFinaMana();
    }
}
