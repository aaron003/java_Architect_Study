package B_Mode.proxy.Dynamic;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/29 21:21
 * @Description : 资金业务-贷款
 */
public class FinaLoan implements IFina {

    public void insertFinaLond() {
        System.out.println("新增贷款业务");
    }

    public void insertFinaMana() {
        System.out.println("新增存放业务");
    }
}
