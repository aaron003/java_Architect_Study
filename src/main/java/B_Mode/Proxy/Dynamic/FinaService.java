package B_Mode.proxy.Dynamic;

import B_Mode.proxy.Dynamic.IFina;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.lang.reflect.Proxy;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/29 21:31
 * @Description : TODO
 */
public class FinaService implements InvocationHandler {
    private IFina iFina;

    public IFina getInstance(IFina iFina){
        this.iFina = iFina;
        Class<?> clazz = iFina.getClass();
        return (IFina) Proxy.newProxyInstance(clazz.getClassLoader(),clazz.getInterfaces(),this);
    }

    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        System.out.println("初始化资金业务");
        Object result = method.invoke(this.iFina,args);
        System.out.println("同步增加资金业务-"+iFina.getClass()+"明细记录");
        return result;
    }
}
