package B_Mode.proxy.Static;


/**
 * @Author : Aaron
 * @Date : Created in 2020/4/29 21:25
 * @Description : 静态代理模式测试类
 */
public class Test {
    public static void main(String[] args) {
        IFina finaLoan = new FinaLoan();
        finaLoan.insertService();
    }
}
