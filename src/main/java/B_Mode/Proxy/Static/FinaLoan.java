package B_Mode.proxy.Static;


/**
 * @Author : Aaron
 * @Date : Created in 2020/4/29 21:21
 * @Description : 资金业务-贷款
 */
public class FinaLoan implements IFina {

    public void initService() {
        System.out.println("初始化贷款业务");
    }

    public void insertService() {
        initService();
        System.out.println("新增贷款业务");
        insertDetails();
    }

    public void insertDetails() {
        System.out.println("贷款业务明细记录同步增加");
    }
}
