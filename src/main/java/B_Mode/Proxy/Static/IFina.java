package B_Mode.proxy.Static;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/29 18:46
 * @Description : 资金业务管理
 */
public interface IFina {
    void initService();

    void insertService();

    void insertDetails();
}
