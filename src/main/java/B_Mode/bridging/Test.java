package B_Mode.bridging;

/**
 * @Author : Aaron
 * @Date : Created in 2020/5/10 23:34
 * @Description : TODO
 */
public class Test {
    public static void main(String[] args) {
        Car car = new Car();
        ICarBrand carBrand = new CarBrandBenz();
        AbstractColor abstractColor = new CarColorBlack();
        abstractColor.setCarBrand(carBrand);
        car.setBrand(abstractColor.getCarBrand().getBrand());
        car.setColor(abstractColor.getColor());
        System.out.println(car.toString());
    }
}
