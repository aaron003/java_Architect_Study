package B_Mode.bridging;

/**
 * @Author : Aaron
 * @Date : Created in 2020/5/10 23:32
 * @Description : TODO
 */
public abstract class AbstractColor implements ICarColor{
    ICarBrand carBrand;

    public void setCarBrand(ICarBrand carBrand) {
        this.carBrand = carBrand;
    }

    public ICarBrand getCarBrand() {
        return carBrand;
    }
}
