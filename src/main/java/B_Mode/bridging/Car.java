package B_Mode.bridging;

import lombok.Data;

/**
 * @Author : Aaron
 * @Date : Created in 2020/5/10 23:27
 * @Description : TODO
 */
@Data
public class Car {
    private String brand;
    private String color;

    @Override
    public String toString() {
        return "汽车{" +
                "品牌='" + this.brand + '\'' +
                ", 颜色='" + this.color + '\'' +
                '}';
    }
}
