package B_Mode.bridging;

/**
 * @Author : Aaron
 * @Date : Created in 2020/5/10 23:26
 * @Description : 车品牌
 */
public interface ICarBrand {
    String getBrand();
}
