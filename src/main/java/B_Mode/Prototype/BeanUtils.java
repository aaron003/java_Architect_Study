package B_Mode.prototype;

import java.lang.reflect.Field;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/12 20:08
 * @Description : 软编码实现原型模式
 */
public class BeanUtils {
    public static Object clone(Object prototype){
        Class clazz = prototype.getClass();
        Object returnType = null;
        try {
            returnType = clazz.newInstance();
            for(Field field : clazz.getDeclaredFields()){
                field.setAccessible(true);
                field.set(returnType,field.get(prototype));
            }
        }catch (Exception e){
            e.printStackTrace();;
        }
        return returnType;
    }
}
