package B_Mode.prototype;


import java.util.ArrayList;
import java.util.List;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/12 19:36
 * @Description : 原型模式测试类
 */
public class Test {

    public static void main(String[] args) {
        ConcretePrototype concretePrototype = new ConcretePrototype();
        concretePrototype.setAge(22);
        concretePrototype.setName("王鹤");
        List<String> hobbys = new ArrayList<String>();
        hobbys.add("唱歌");
        hobbys.add("出去玩");
        concretePrototype.setHobby(hobbys);
        ConcretePrototype concretePrototype1 = concretePrototype.jsonByDeepClone();
        concretePrototype1.setAge(18);
        concretePrototype1.getHobby().add("打游戏");


        System.out.println(concretePrototype.toString());
        System.out.println(concretePrototype1.toString());
        System.out.println(concretePrototype == concretePrototype1);
        System.out.println(concretePrototype.getHobby() == concretePrototype1.getHobby());
    }
}

