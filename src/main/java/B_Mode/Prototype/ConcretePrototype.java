package B_Mode.prototype;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/12 19:46
 * @Description : 原型模式
 */
public class ConcretePrototype implements Cloneable,Serializable{
    private int age;
    private String name;
    private List<String> hobby;

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<String> getHobby() {
        return hobby;
    }

    public void setHobby(List<String> hobby) {
        this.hobby = hobby;
    }

    /**
    * create by : aaron
    * descriotion : 浅克隆方法
    * create time : 2020/4/12 21:46
    */
    public ConcretePrototype clone(){
        try {
            return (ConcretePrototype) super.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
    * create by : aaron
    * descriotion : 深克隆序列化方式
    * create time : 2020/4/12 21:44
    */
    public ConcretePrototype serByDeepClone(){
        ByteArrayOutputStream bos = null;
        ObjectOutputStream oos = null;
        ByteArrayInputStream bis = null;
        ObjectInputStream ois = null;
        try {
            bos = new ByteArrayOutputStream();
            oos = new ObjectOutputStream(bos);
            oos.writeObject(this);

            bis = new ByteArrayInputStream(bos.toByteArray());
            ois = new ObjectInputStream(bis);

            return (ConcretePrototype)ois.readObject();
        }catch (Exception e){
            e.printStackTrace();
            return null;
        }finally {
            try {
                bos.close();
                oos.close();
                bis.close();
                ois.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
    * create by : aaron
    * descriotion : 深克隆强转方式
    * create time : 2020/4/12 22:18
    */
    public ConcretePrototype listByDeepClone(){
        try {
            ConcretePrototype result = (ConcretePrototype) super.clone();
            result.hobby = (List)((ArrayList)result.hobby).clone();
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }
    /**
    * create by : aaron
    * descriotion : 深克隆JSON方式
    * create time : 2020/4/12 21:48
    */
    public ConcretePrototype jsonByDeepClone(){
        return JSONObject.parseObject(JSONObject.toJSONString(this),ConcretePrototype.class);
    }

    @Override
    public String toString() {
        return "ConcretePrototype{" +
                "age=" + age +
                ", name='" + name + '\'' +
                ", hobby=" + hobby +
                '}';
    }
}
