package B_Mode.adapter;

import org.apache.commons.lang.RandomStringUtils;

/**
 * @Author : Aaron
 * @Date : Created in 2020/5/10 18:32
 * @Description : TODO
 */
public class PassportForThirdAdapter implements IPassportForThird {
    public void regist(String username, String password) {
        System.out.println("注册成功，账号为：" + username + "，密码为：" + password);
    }

    public void login(String username, String password) {
        System.out.println("登录成功，账号为：" + username + "，密码为：" + password);
    }

    public void loginForThird(String id, Class<? extends ILoginAdapter> clazz) {
        try {
            ILoginAdapter adapter = clazz.newInstance();
            if (adapter.support(adapter)){
                adapter.login(id,adapter);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
