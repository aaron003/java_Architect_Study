package B_Mode.adapter;

/**
 * @Author : Aaron
 * @Date : Created in 2020/5/10 18:22
 * @Description : 登录接口
 */
public interface ILoginAdapter {
    boolean support(Object adapter);

    void login(String id, Object adapter);
}
