package B_Mode.adapter;

/**
 * @Author : Aaron
 * @Date : Created in 2020/5/10 18:34
 * @Description : TODO
 */
public class LoginForQQ extends AbstractAdapter {
    public boolean support(Object adapter) {
        return adapter instanceof LoginForQQ;
    }

    public void login(String id, Object adapter) {
        if (!support(adapter)){
            return;
        }
        super.loginForRegist(id);
    }
}
