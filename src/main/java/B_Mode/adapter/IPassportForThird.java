package B_Mode.adapter;

/**
 * @Author : Aaron
 * @Date : Created in 2020/5/10 18:32
 * @Description : TODO
 */
public interface IPassportForThird {
    void regist(String id,String password);

    void login(String id, String password);

    void loginForThird(String id, Class<? extends ILoginAdapter> clazz);
}
