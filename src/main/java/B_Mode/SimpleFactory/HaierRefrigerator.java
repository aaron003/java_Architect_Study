package B_Mode.simpleFactory;


/**
 * @Author : Aaron
 * @Date : Created in 2020/4/10 17:45
 * @Description : 海尔冰箱
 */
public class HaierRefrigerator implements IHaierElectric {
    public void produce(){
        System.out.println("海尔冰箱生产线开始运作...");
        System.out.println("海尔冰箱生产开始...");
        System.out.println("海尔冰箱生产完毕!!!");
    }
}
