package B_Mode.simpleFactory;


/**
 * @Author : Aaron
 * @Date : Created in 2020/4/10 17:42
 * @Description : 海尔电器工厂类
 */
public class HaierElectricFactory {
    public IHaierElectric dispatch(Class<? extends IHaierElectric> className) {
        try {
            if (!(null == className)) {
                return className.newInstance();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
