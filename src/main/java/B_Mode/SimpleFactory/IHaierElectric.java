package B_Mode.simpleFactory;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/10 17:44
 * @Description : 海尔电器生产线总接口
 */
public interface IHaierElectric {
    void produce();
}
