package B_Mode.simpleFactory;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/10 18:07
 * @Description : 简单工厂测试类
 */
public class Test {
    public static void main(String[] args) {
        IHaierElectric iHaierElectric = new HaierElectricFactory().dispatch(HaierAirConditioning.class);
        iHaierElectric.produce();
    }
}
