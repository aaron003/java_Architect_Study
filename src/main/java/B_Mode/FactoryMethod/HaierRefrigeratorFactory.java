package B_Mode.factoryMethod;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/10 17:42
 * @Description : 海尔冰箱工厂类
 */
public class HaierRefrigeratorFactory implements IHaierElectricFactory{

    public IHaierElectric dispatch(){
        return new HaierRefrigerator();
    }
}
