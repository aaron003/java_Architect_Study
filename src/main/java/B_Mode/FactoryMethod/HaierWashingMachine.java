package B_Mode.factoryMethod;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/10 17:46
 * @Description : 海尔洗衣机
 */
public class HaierWashingMachine implements IHaierElectric {
    public void produce1() {
        System.out.println("海尔洗衣机生产线1开始运作...");
        System.out.println("海尔洗衣机生产开始...");
        System.out.println("海尔洗衣机生产完毕!!!");
    }

    public void produce2() {
        System.out.println("海尔洗衣机生产线2开始运作...");
        System.out.println("海尔洗衣机生产开始...");
        System.out.println("海尔洗衣机生产完毕!!!");
    }

    public void produce3() {
        System.out.println("海尔洗衣机生产线3开始运作...");
        System.out.println("海尔洗衣机生产开始...");
        System.out.println("海尔洗衣机生产完毕!!!");
    }
}
