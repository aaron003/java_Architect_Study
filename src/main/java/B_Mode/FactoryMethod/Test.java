package B_Mode.factoryMethod;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/10 18:09
 * @Description : 工厂方法测试类
 */
public class Test {
    public static void main(String[] args) {
        IHaierElectricFactory iHaierElectricFactory = new HaierTVFactory();
        IHaierElectric iHaierElectric = iHaierElectricFactory.dispatch();
        iHaierElectric.produce2();
    }
}
