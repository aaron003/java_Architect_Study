package B_Mode.factoryMethod;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/10 17:46
 * @Description : 海尔洗衣机工厂类
 */
public class HaierWashingMachineFactory implements IHaierElectricFactory{

    public IHaierElectric dispatch(){
        return new HaierWashingMachine();
    }
}
