package B_Mode.factoryMethod;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/10 17:44
 * @Description : 海尔电视机
 */
public class HaierTV implements IHaierElectric {
    public void produce1() {
        System.out.println("海尔电视机生产线1开始运作...");
        System.out.println("海尔电视机生产开始...");
        System.out.println("海尔电视机生产完毕!!!");
    }

    public void produce2() {
        System.out.println("海尔电视机生产线2开始运作...");
        System.out.println("海尔电视机生产开始...");
        System.out.println("海尔电视机生产完毕!!!");
    }

    public void produce3() {
        System.out.println("海尔电视机生产线3开始运作...");
        System.out.println("海尔电视机生产开始...");
        System.out.println("海尔电视机生产完毕!!!");
    }
}
