package B_Mode.factoryMethod;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/10 17:44
 * @Description : 海尔电视机工厂类
 */
public class HaierTVFactory implements IHaierElectricFactory{

    public IHaierElectric dispatch(){
        return new HaierTV();
    }
}
