package B_Mode.factoryMethod;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/10 18:24
 * @Description : 海尔电器工厂总接口
 */
public interface IHaierElectricFactory {
    public IHaierElectric dispatch();
}
