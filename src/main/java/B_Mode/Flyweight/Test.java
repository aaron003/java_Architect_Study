package B_Mode.flyweight;

import java.sql.Connection;

/**
 * @Author : Aaron
 * @Date : Created in 2020/5/9 17:20
 * @Description : TODO
 */
public class Test {
    public static void main(String[] args) {
        ConnectionPool pool = new ConnectionPool();
        Connection conn = pool.getConnection();
        System.out.println(conn);
        pool.recycle(conn);
    }
}
