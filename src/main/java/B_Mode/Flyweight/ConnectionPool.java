package B_Mode.flyweight;

import B_Mode.builder.Map;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.Vector;

/**
 * @Author : Aaron
 * @Date : Created in 2020/5/9 17:06
 * @Description : TODO
 */
public class ConnectionPool {
    private Vector<Connection> pool;
    private int poolSize = 10;

    public ConnectionPool() {
        pool = new Vector<Connection>(poolSize);

        try {
            Class.forName("com.mysql.jdbc.Driver");
            for (int i = 0 ; i< poolSize;i++){
                Connection conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/distribution?useUnicode=true&characterEncoding=utf8","root","root");
                pool.add(conn);
            }
            System.out.println("数据库连接成功");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public synchronized Connection getConnection(){
        if (pool.size()>0){
            Connection conn = pool.get(0);
            pool.remove(0);
            return conn;
        }
        return null;
    }

    public synchronized void recycle(Connection conn){
        pool.add(conn);
    }
}
