package B_Mode.singleton;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/12 1:07
 * @Description : TODO
 */
public class Lazy {
    private static Lazy instance = null;

    private Lazy() {
    }

    public static Lazy getInstance() {
        if (null == instance)instance = new Lazy();
        return instance;
    }
}
