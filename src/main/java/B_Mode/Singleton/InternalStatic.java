package B_Mode.singleton;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/12 15:38
 * @Description : 内部静态类单例
 */
public class InternalStatic {

    private InternalStatic(){}

    public static class InstanceHolder{
        private final static InternalStatic instance = new InternalStatic();
    }

    public static InternalStatic getInstance(){
        return InstanceHolder.instance;
    }
}
