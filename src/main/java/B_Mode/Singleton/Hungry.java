package B_Mode.singleton;

import java.io.Serializable;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/12 0:50
 * @Description : 饿汉式单例
 */
public class Hungry {
    private static final Hungry instance = new Hungry();

    private Hungry(){
    }

    public static Hungry getInstanle(){
        return instance;
    }

}
