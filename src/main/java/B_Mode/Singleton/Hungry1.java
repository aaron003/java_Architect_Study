package B_Mode.singleton;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/12 0:50
 * @Description : 饿汉式单例1
 */
public class Hungry1 {
    private static final Hungry1 instance ;

    static {
        instance = new Hungry1();
    }

    private Hungry1(){}

    public static Hungry1 getInstanle(){
        return instance;
    }
}
