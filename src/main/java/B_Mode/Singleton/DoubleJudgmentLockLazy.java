package B_Mode.singleton;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/12 1:23
 * @Description : 双重判断加锁懒汉式单例
 */
public class DoubleJudgmentLockLazy {
    private volatile static DoubleJudgmentLockLazy instance = null;

    private DoubleJudgmentLockLazy(){}

    public static DoubleJudgmentLockLazy getInstance(){
        if (null == instance){
            synchronized (DoubleJudgmentLockLazy.class){
                if (null == instance){
                    instance = new DoubleJudgmentLockLazy();
                    return instance;
                }
            }
        }
        return instance;
    }
}
