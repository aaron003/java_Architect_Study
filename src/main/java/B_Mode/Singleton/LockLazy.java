package B_Mode.singleton;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/12 1:16
 * @Description : 加锁懒汉式单例
 */
public class LockLazy {
    private static LockLazy instance = null;

    private LockLazy(){}

    public synchronized static LockLazy getInstance(){
        if (null == instance)instance = new LockLazy();
        return instance;
    }
}
