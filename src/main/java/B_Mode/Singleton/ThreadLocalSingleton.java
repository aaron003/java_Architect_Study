package B_Mode.singleton;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/12 19:27
 * @Description : ThreadLocal式单例
 */
public class ThreadLocalSingleton {
    private static final ThreadLocal<ThreadLocalSingleton> instance = new ThreadLocal<ThreadLocalSingleton>(){
        @Override
        protected ThreadLocalSingleton initialValue() {
            return new ThreadLocalSingleton();
        }
    };

    private ThreadLocalSingleton(){}

    public static ThreadLocalSingleton getInstance(){return instance.get();}
}
