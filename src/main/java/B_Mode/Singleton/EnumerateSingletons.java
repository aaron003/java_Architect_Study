package B_Mode.singleton;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/12 17:54
 * @Description : TODO
 */
public enum  EnumerateSingletons {
    INSTANCE;

    private Object data;

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

    public static EnumerateSingletons getInstance(){return INSTANCE;}
}
