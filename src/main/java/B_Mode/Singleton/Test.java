package B_Mode.singleton;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/12 0:51
 * @Description : 单例模式测试类
 */
public class Test {
    @org.junit.Test
    /**
     * create by : aaron
     * descriotion : 饿汉式单例
     * create time : 2020/4/12 0:51
     * @return : []
     */
    public void test1() {
        Hungry hungry1 = Hungry.getInstanle();
        Hungry hungry2 = Hungry.getInstanle();
        System.out.println(hungry1 == hungry2);//true
        Hungry1 hungry11 = Hungry1.getInstanle();
        Hungry1 hungry12 = Hungry1.getInstanle();
        System.out.println(hungry11 == hungry12);//true
    }

    @org.junit.Test
    /**
     * create by : aaron
     * descriotion : 懒汉式单例  用多线程跑的情况下结果可能会是false
     * create time : 2020/4/12 1:05
     * @return : []
     */
    public void test2() {
        Lazy lazy = Lazy.getInstance();
        Lazy lazy1 = Lazy.getInstance();
        System.out.println(lazy == lazy1);//true
    }

    @org.junit.Test
    /**
     * create by : aaron
     * descriotion : 加锁懒汉式单例 用多线程跑的情况下结果也是true  当线程多时会降低执行效率
     * create time : 2020/4/12 1:05
     * @return : []
     */
    public void test3() {
        LockLazy lockLazy1 = LockLazy.getInstance();
        LockLazy lockLazy2 = LockLazy.getInstance();
        System.out.println(lockLazy1 == lockLazy2);//true
        LockLazy1 lockLazy11 = LockLazy1.getInstance();
        LockLazy1 lockLazy12 = LockLazy1.getInstance();
        System.out.println(lockLazy11 == lockLazy12);//true
    }

    @org.junit.Test
    /**
     * create by : aaron
     * descriotion : 双重加锁懒汉式单例 用多线程跑的情况下结果也是true
     * create time : 2020/4/12 1:05
     * @return : []
     */
    public void test4() {
        LockLazy lockLazy1 = LockLazy.getInstance();
        LockLazy lockLazy2 = LockLazy.getInstance();
        System.out.println(lockLazy1 == lockLazy2);//true
        LockLazy1 lockLazy11 = LockLazy1.getInstance();
        LockLazy1 lockLazy12 = LockLazy1.getInstance();
        System.out.println(lockLazy11 == lockLazy12);//true
    }

    @org.junit.Test
    /**
     * create by : aaron
     * descriotion : 内部静态类单例
     * create time : 2020/4/12 1:05
     * @return : []
     */
    public void test5() {
        InternalStatic internalStatic = InternalStatic.getInstance();
        InternalStatic internalStatic1 = InternalStatic.getInstance();
        System.out.println(internalStatic == internalStatic1);//true
    }

    @org.junit.Test
    /**
     * create by : aaron
     * descriotion : 枚举注册式单例
     * create time : 2020/4/12 1:05
     * @return : []
     */
    public void test6() {
        EnumerateSingletons enumerateSingletons = EnumerateSingletons.getInstance();
        EnumerateSingletons enumerateSingletons1 = EnumerateSingletons.getInstance();
        System.out.println(enumerateSingletons == enumerateSingletons1);//true
    }

}
