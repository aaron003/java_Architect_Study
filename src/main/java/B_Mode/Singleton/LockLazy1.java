package B_Mode.singleton;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/12 1:16
 * @Description : 加锁懒汉式单例
 */
public class LockLazy1 {
    private static LockLazy1 instance = null;

    private LockLazy1(){}

    public static LockLazy1 getInstance(){
        synchronized (LockLazy1.class){
            if (null == instance)instance = new LockLazy1();
            return instance;
        }
    }
}
