package A_Principles.SimpleResponsibility_InterfaceSegregation;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/8 17:34
 * @Description : 汽车发动机
 * @Version : 1.0
 */
public interface IEngine{
    public void assembnledEngine(String engine);
}
