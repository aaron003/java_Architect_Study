package A_Principles.SimpleResponsibility_InterfaceSegregation;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/9 19:57
 * @Description : 组装车实现类
 * @Version : 1.0
 */
public class AssembledCar implements ICarBody,IChassis,IElectricalEquipment,IEngine{
    Car car;

    public AssembledCar(Car car) {
        this.car = car;
    }

    public void assembnledCarBody(String carBody) {
        car.setCarBody(carBody);
    }

    public void assembnledChassis(String chassis) {
        car.setChassis(chassis);
    }

    public void assembnledElectricalEquipment(String electricalEquipment) {
        car.setElectricalEquipment(electricalEquipment);
    }

    public void assembnledEngine(String engine) {
        car.setEngine(engine);
    }
}
