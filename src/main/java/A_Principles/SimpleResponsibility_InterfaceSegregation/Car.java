package A_Principles.SimpleResponsibility_InterfaceSegregation;



/**
 * @Author : Aaron
 * @Date : Created in 2020/4/9 19:58
 * @Description : 车POJO
 * @Version : 1.0
 */
public class Car {
    private String carBody;
    private String chassis;
    private String electricalEquipment;
    private String engine;

    public String getCarBody() {
        return carBody;
    }

    public void setCarBody(String carBody) {
        this.carBody = carBody;
    }

    public String getChassis() {
        return chassis;
    }

    public void setChassis(String chassis) {
        this.chassis = chassis;
    }

    public String getElectricalEquipment() {
        return electricalEquipment;
    }

    public void setElectricalEquipment(String electricalEquipment) {
        this.electricalEquipment = electricalEquipment;
    }

    public String getEngine() {
        return engine;
    }

    public void setEngine(String engine) {
        this.engine = engine;
    }

    @Override
    public String toString() {
        if(null == carBody || "".equals(carBody))carBody = "还没有组装车身";
        if(null == chassis || "".equals(chassis))chassis = "还没有组装底盘";
        if(null == electricalEquipment || "".equals(electricalEquipment))electricalEquipment = "还没有组装电气设备";
        if(null == engine || "".equals(engine))engine = "还没有组装发动机";
        return "Car{" +
                "carBody='" + carBody + '\'' +
                ", chassis='" + chassis + '\'' +
                ", electricalEquipment='" + electricalEquipment + '\'' +
                ", engine='" + engine + '\'' +
                '}';
    }
}
