package A_Principles.SimpleResponsibility_InterfaceSegregation;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/8 17:35
 * @Description : 汽车底盘
 * @Version : 1.0
 */
public interface IChassis {
    public void assembnledChassis(String chassis);
}
