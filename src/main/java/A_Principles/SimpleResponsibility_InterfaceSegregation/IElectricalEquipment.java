package A_Principles.SimpleResponsibility_InterfaceSegregation;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/8 17:36
 * @Description : 汽车电气设备
 * @Version : 1.0
 */
public interface IElectricalEquipment {
    public void assembnledElectricalEquipment(String electricalEquipment);
}
