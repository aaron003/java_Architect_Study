package A_Principles.SimpleResponsibility_InterfaceSegregation;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/9 20:08
 * @Description : 单一职责_接口测试类   也适用于接口隔离原则
 * @Version : 1.0
 */
public class Test {
    public static void main(String[] args) {
        Car car = new Car();
        AssembledCar assembledCar = new AssembledCar(car);
        assembledCar.assembnledCarBody("宝马车身1级");
        assembledCar.assembnledChassis("宝马车底盘1级");
        assembledCar.assembnledElectricalEquipment("宝马车电气设备1级");
        assembledCar.assembnledEngine("宝马车发动机1级");
        System.out.println(car.toString());
    }
}
