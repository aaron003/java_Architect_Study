package A_Principles.SimpleResponsibility_InterfaceSegregation;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/9 19:03
 * @Description : 汽车车身接口类
 * @Version : 1.0
 */
public interface ICarBody {
    public void assembnledCarBody(String carBody);
}
