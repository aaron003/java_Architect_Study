package A_Principles.DependenceInversion;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/8 17:35
 * @Description : 汽车底盘
 * @Version : 1.0
 */
public class Chassis implements IAssembledCar{
    private final static String CHASSIS="底盘等级";

    public String assembledComponents(int level) {
        return CHASSIS+level;
    }
}
