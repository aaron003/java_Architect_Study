package A_Principles.DependenceInversion;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/8 17:45
 * @Description : 依赖倒置原则测试类
 * （1）高层模块不应该依赖底层模块，二者都应该依赖抽象。 高层模块-》AssembledCar 底层模块-》CarBody
 * （2）抽象不应该依赖细节，细节应该依赖抽象。
 * （3）依赖倒置的中心思想是面向接口编程。
 * @Version : 1.0
 */
public class Test {
    public static void main(String[] args) {
        String car = "";
        AssembledCar assembledCar = new AssembledCar();
        car = car + assembledCar.addembled(new CarBody(),1);
        car = car + assembledCar.addembled(new Chassis(),1);
        car = car + assembledCar.addembled(new Engine(),1);
        System.out.println(car);
    }
}

