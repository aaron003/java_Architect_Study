package A_Principles.DependenceInversion;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/8 17:36
 * @Description : 汽车电气设备
 * @Version : 1.0
 */
public class ElectricalEquipment implements IAssembledCar{
    private final static String ELECTRICALEQUIPMENT="电气设备等级";

    public String assembledComponents(int level) {
        return ELECTRICALEQUIPMENT+level;
    }
}
