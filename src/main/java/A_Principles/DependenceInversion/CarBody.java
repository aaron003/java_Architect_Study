package A_Principles.DependenceInversion;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/8 17:36
 * @Description : 汽车车身
 * @Version : 1.0
 */
public class CarBody implements IAssembledCar{
    private final static String CARBODY="车身等级";

    public String assembledComponents(int level) {
        return CARBODY+level;
    }
}
