package A_Principles.DependenceInversion;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/8 17:24
 * @Description : 组装汽车接口类
 * @Version : 1.0
 */
public interface IAssembledCar {
    public String assembledComponents(int level);
}
