package A_Principles.DependenceInversion;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/8 17:41
 * @Description : 组装汽车实现类
 * @Version : 1.0
 */
public class AssembledCar {

    public String addembled(IAssembledCar assembledCar,int level){
        return assembledCar.assembledComponents(level);
    };
}
