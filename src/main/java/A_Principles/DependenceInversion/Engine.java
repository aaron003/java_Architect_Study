package A_Principles.DependenceInversion;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/8 17:34
 * @Description : 汽车发动机
 * @Version : 1.0
 */
public class Engine implements IAssembledCar{

    private final static String ENGINE="发动机等级";

    public String assembledComponents(int level) {
        return ENGINE+level;
    }
}
