package A_Principles.LiakovSubstitution.Mode1;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/10 14:49
 * @Description : 正方形
 */
public class Square implements QuadRangle{
    private long length;

    public long getLength() {
        return length;
    }

    public void setLength(long length) {
        this.length = length;
    }

    public long getHeight() {
        return length;
    }

    public long getWidth() {
        return length;
    }
}
