package A_Principles.LiakovSubstitution.Mode1;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/10 14:49
 * @Description : 长方形
 */
public class Rectangle implements QuadRangle{
    private long height;
    private long width;

    public long getHeight() {
        return height;
    }

    public void setHeight(long height) {
        this.height = height;
    }

    public long getWidth() {
        return width;
    }

    public void setWidth(long width) {
        this.width = width;
    }
}
