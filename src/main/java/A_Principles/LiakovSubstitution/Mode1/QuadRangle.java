package A_Principles.LiakovSubstitution.Mode1;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/10 14:48
 * @Description : 四边形接口类
 */
public interface QuadRangle {
    long getHeight();
    long getWidth();
}
