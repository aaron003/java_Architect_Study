package A_Principles.LiakovSubstitution;

import A_Principles.LiakovSubstitution.Mode1.QuadRangle;
import A_Principles.LiakovSubstitution.Mode1.Rectangle;
import A_Principles.LiakovSubstitution.Mode2.Base;
import A_Principles.LiakovSubstitution.Mode2.Child;

import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/10 13:52
 * @Description : 里氏替换原则测试类
 * @Version : 1.0
 */
public class Test {
    @org.junit.Test
    /**
    * create by : aaron
    * descriotion : 强制某些方法  子类和父类不能随意替换  不用继承用实现 实现类可以共有的接口
    * create time : 2020/4/10 14:42
    * @return : []
    */
    public void test1(){
        Rectangle rectangle = new Rectangle();
        rectangle.setWidth(20);
        rectangle.setHeight(10);
        while (rectangle.getWidth()>=rectangle.getHeight()){
            rectangle.setHeight(rectangle.getHeight()+1);
            System.out.println("长方形宽:"+rectangle.getWidth()+",高:"+rectangle.getHeight());
        }
        System.out.println("whileEnd,长方形宽:"+rectangle.getWidth()+",高:"+rectangle.getHeight());
    }

    @org.junit.Test
    /**
    * create by : aaron
    * descriotion : 子类入参类型大于父类入参类型
     *              子类返回类型小于父类返回类型
    * create time : 2020/4/10 14:43
    * @return : []
    */
    public void test2(){
        Base base = new Base();
        base.method();
        Child child = new Child();
        child.method();

    }

}
