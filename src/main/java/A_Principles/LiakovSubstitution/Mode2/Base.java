package A_Principles.LiakovSubstitution.Mode2;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/10 17:08
 * @Description : 父类
 */
public class Base {
    public void method(HashMap hashMap){
        System.out.println("父类执行入参HashMap方法!!!");
    }

    public Map method(){
        System.out.println("父类执行返回Map方法!!!");
        return new HashMap();
    }
}
