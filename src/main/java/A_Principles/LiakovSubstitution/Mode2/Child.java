package A_Principles.LiakovSubstitution.Mode2;

import java.util.HashMap;
import java.util.Map;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/10 17:09
 * @Description : 子类
 */
public class Child extends Base{

    public void method(Map hashMap) {
        System.out.println("子类执行入参Map方法!!!");
    }

    public HashMap method(){
        System.out.println("子类执行返回HashMap方法!!!");
        return new HashMap();
    }
}
