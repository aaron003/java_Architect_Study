package A_Principles.OpenClosed.v1;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/5 19:24
 * @Description : 汽车接口
 * @Version : 1.0
 */
public interface ICar {
    public String carName();
    public double price();
}
