package A_Principles.OpenClosed.v1;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/5 19:25
 * @Description : 开闭原则测试类
 * @Version : 1.0
 */
public class Test {
    public static void main(String[] args) {
        ICar car = new CarPromotion();
        System.out.println(car.toString());
    }
}
