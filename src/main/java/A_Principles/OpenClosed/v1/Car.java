package A_Principles.OpenClosed.v1;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/5 19:26
 * @Description : 汽车实现类
 * @Version : 1.0
 */
public class Car implements ICar{

    public String carName() {
        return "宝马i8";
    }

    public double price() {
        return 200;
    }

    @Override
    public String toString() {
        return "汽车型号:"+carName()+",汽车售价:"+price()+"万";
    }
}
