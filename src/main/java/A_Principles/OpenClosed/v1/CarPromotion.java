package A_Principles.OpenClosed.v1;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/5 19:32
 * @Description : 汽车销售促销类
 * @Version : 1.0
 */
public class CarPromotion extends Car{
    @Override
    public double price() {
        return super.price() * 0.8;
    }

    @Override
    public String toString() {
        return "汽车型号:"+carName()+",汽车原售价:"+super.price()+"万,汽车促销售价:"+price()+"万";
    }
}
