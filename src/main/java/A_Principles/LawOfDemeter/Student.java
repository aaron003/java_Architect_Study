package A_Principles.LawOfDemeter;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/9 21:03
 * @Description : 学生类
 * @Version : 1.0
 */
public class Student {
    private String classId;
    private String name;
    private int age;
    private String address;
    private String gender;

    public String getClassId() {
        return classId;
    }

    public void setClassId(String classId) {
        this.classId = classId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }
}
