package A_Principles.LawOfDemeter;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/9 21:02
 * @Description : 学校类
 * @Version : 1.0
 */
public class School {
    public void checkClassNumber(Class clazz) {
        List<Class> schools = new ArrayList<Class>();
        for (int i = 0; i < 20; i++) {
            schools.add(new Class());
        }
        System.out.println("该学校有"+schools.size()+"个班级!!!");
        clazz.checkStudentNumber();
    }
}
