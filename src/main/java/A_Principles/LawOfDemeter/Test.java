package A_Principles.LawOfDemeter;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/9 20:38
 * @Description : 迪米特法则也叫最少知道原则
 * @Version : 1.0
 */
public class Test {
    public static void main(String[] args) {
        School school = new School();
        Class clazz = new Class();
        school.checkClassNumber(clazz);
    }
}
