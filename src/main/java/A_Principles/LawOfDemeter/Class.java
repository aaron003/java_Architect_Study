package A_Principles.LawOfDemeter;

import java.util.ArrayList;
import java.util.List;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/9 21:03
 * @Description : 班级类
 * @Version : 1.0
 */
public class Class {
    public void checkStudentNumber() {
        List<Student> clazzs = new ArrayList<Student>();
        for (int i = 0; i < 40; i++) {
            clazzs.add(new Student());
        }
        System.out.println("每个班级有学生:"+clazzs.size()+"名");
    }
}
