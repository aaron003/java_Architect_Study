package A_Principles.CompositeAggregateReuse;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/10 17:20
 * @Description : 合成服用原则
 */
public class Test {
    public static void main(String[] args) {
        Computer computer = new Computer();
        computer.installation();
    }

}
