package A_Principles.CompositeAggregateReuse;

/**
 * @Author : Aaron
 * @Date : Created in 2020/4/10 17:21
 * @Description : 电脑类
 */
public class Computer {
    Mouse mouse = new Mouse();
    Keyboard keyboard = new Keyboard();
    private  Audio audio ;

    public Computer(){
        audio = new Audio();
    }

    public void installation(){
        mouse.add();
        keyboard.add();
        //audio.add();
    }
}
